Pod::Spec.new do |s|
  s.name         = "RKSwipe"
  s.version      = "0.1.4"
  s.summary      = "Swipe between ViewControllers like in the Spotify or Twitter app with an interactive Segmented Control in the Navigation Bar"

  s.description  = "Similar to Twitter and Spotify, swipe between view controllers and the tabs in the navigation bar changes. -twitter, -spotify, -swipe, -navigation bar, -navigationbar, -between, -view controllers, -viewcontroller, -tab, -objectivec, -ios, -iphone, -xcode"

  s.homepage     = "https://bitbucket.org/omarjalil/rkswipe"
  s.screenshots  = "http://i.imgur.com/zEsm542.gif"

  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author    = "cwrichardkim"
  s.social_media_url   = "http://twitter.com/cwrichardkim"

  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/omarjalil/rkswipe.git" }
  s.source_files  = 'RKSwipeBetweenViewControllers.h', 'RKSwipeBetweenViewControllers.m', 'RKSwipeCrearBitacoras.h', 'RKSwipeCrearBitacoras.m'
  s.requires_arc = true
  
end