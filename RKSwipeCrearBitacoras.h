//
//  RKSwipeCrearBitacoras.h
//  Biobot
//
//  Created by biobot.farm on 26/04/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RKSwipeCrearBitacorasDelegate <NSObject>

@end

@interface RKSwipeCrearBitacoras : UINavigationController <UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *viewControllerArray;
@property (nonatomic, weak) id<RKSwipeCrearBitacorasDelegate> navDelegate;
@property (nonatomic, strong) UIView *selectionBar;
@property (nonatomic, strong)UIPageViewController *pageController;
@property (nonatomic, strong)UIView *navigationView;
@property (nonatomic, strong)NSArray *buttonText;

@end
